// console.log("Hello World");

// >> Synchronous Proof
// JavaScript is by default synchronous, meaning that it only executes one statement at a time

/*
console.log("Hello World p.2");
conosle.log("Hello again");
console.log("Goodbye");
*/
// Code blocking - waiting for the specific statement to finish before executing the next statement

/*for(let i=0; i<=1500; i++){
	console.log(i);
}
console.log("Hello again");*/
//finished the loop before posting the console


// Asynchronous means that we can proceed to execute other statement, while time consuming code is running in the background

// The Fetch API that allows us to asynchronously request for a resource (data).
// "fetch()" method in JS is used to request to the server and load info on the webpages
// SYNTAX:
	// fetch("APIURL");

//[SECTION] Getting all posts

fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => console.log(res.json()));
// ".then()" method captures the response object and returns a promise which will be "fulfilled" or "rejected"
// ".json()" identifies a network response and converts it directly to JS objects.

fetch("https://jsonplaceholder.typicode.com/posts")
//.then(res => console.log(res))
.then(res => console.log(res.status))

fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => res.json()) // returns a response that is converted into a jS objects
.then(response => console.log(response)); // display the fulfilled response from the previous .then()

// Translating arrow function to traditional function
/*
fetch("https://jsonplaceholder.typicode.com/posts")
.then(function(res){ 
	return res.json()
})
.then(function(response){ 
	return console.log(response)
})
*/

// [SECTION] Getting a specific post
// ":id" is a wildcard where you can put any value it then creates a link between "id" parameter in the URL and value provided in the URL
/*
	fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(res => res.json())
	.then(response => console.log(response))
*/


// The async and await keyword to achieve asynchronous code.
async function fetchData(){
	let result = await(fetch("https://jsonplaceholder.typicode.com/posts/1"))
	console.log(result);

	let json = await result.json();
	console.log(json);

	console.log("hello world");
}

fetchData();

// GET, POST, PUT, PATCH DELETE

// [GET] Getting a specific docs

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json())
.then(response => console.log(response))

// [POST] Inserting a docs /field
/*
	Syntax:
		fetch("apiURL", {options})
		.then((response) => {})
		.then((response) => {})
*/ 

fetch("https://jsonplaceholder.typicode.com/posts", 
{
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId:1
	})
})
.then(response => response.json())
.then(json => console.log(json))

//[PUT] Updating a whole docs (all fields)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		title: "updated post",
		body: "Hello again",
		userId:1
	})

})
.then(response => response.json())
.then(json => console.log(json))



// [PATCH] Updating a specific fiels of a docs
// PUT vs PATCH
	// PATCH is used to update a single or several properties
	// PUT is used to update the whole docs/object

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title"
	})
})
.then(response => response.json())
.then(json => console.log(json))



// [DELETE] Deleting a docs
fetch("https://jsonplaceholder.typicode.com/posts/1",
	{
		method: "DELETE",
	}
).then(response => response.json())
.then(json => console.log(json))


// Additional code: 

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});

